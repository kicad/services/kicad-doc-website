################################

FROM ruby:3.3 AS site-build-env

WORKDIR /site

RUN date

RUN curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg
RUN NODE_MAJOR=20 && echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_$NODE_MAJOR.x nodistro main" | tee /etc/apt/sources.list.d/nodesource.list
RUN apt-get update && apt-get install -y nodejs

# install gems
COPY Gemfile Gemfile.lock ./
RUN gem install bundler
RUN bundle install

#copy the entire website folder into the build environment container
COPY . .

COPY --from=registry.gitlab.com/kicad/services/kicad-doc:5.1 /src /site/kicad-doc-built/5.1
COPY --from=registry.gitlab.com/kicad/services/kicad-doc:6.0 /src /site/kicad-doc-built/6.0
COPY --from=registry.gitlab.com/kicad/services/kicad-doc:7.0 /src /site/kicad-doc-built/7.0
COPY --from=registry.gitlab.com/kicad/services/kicad-doc:8.0 /src /site/kicad-doc-built/8.0
COPY --from=registry.gitlab.com/kicad/services/kicad-doc:9.0 /src /site/kicad-doc-built/9.0
COPY --from=registry.gitlab.com/kicad/services/kicad-doc:master /src /site/kicad-doc-built/master

#actually build the site
RUN rake process

RUN JEKYLL_ENV=production jekyll build

######################################

# lets create the actual deployment image
FROM nginx:alpine

#copy over the site config for nginx
COPY ./.docker/default.conf /etc/nginx/conf.d/default.conf
COPY ./.docker/kicad-downloads-proxy-pass.conf /etc/nginx/conf.d/kicad-downloads-proxy-pass.conf

#copy over the built website from the build environment docker
COPY --from=site-build-env /site/_site /usr/share/nginx/html

#copy the doxygen docs
COPY --from=registry.gitlab.com/kicad/code/kicad/doxygen:master /doxygen-docs_html /usr/share/nginx/html/doxygen
COPY --from=registry.gitlab.com/kicad/code/kicad/doxygen:master /doxygen-python_html /usr/share/nginx/html/doxygen-python-nightly
COPY --from=registry.gitlab.com/kicad/code/kicad/doxygen:6.0 /doxygen-python_html /usr/share/nginx/html/doxygen-python-6.0
COPY --from=registry.gitlab.com/kicad/code/kicad/doxygen:7.0 /doxygen-python_html /usr/share/nginx/html/doxygen-python-7.0
COPY --from=registry.gitlab.com/kicad/code/kicad/doxygen:8.0 /doxygen-python_html /usr/share/nginx/html/doxygen-python-8.0
COPY --from=registry.gitlab.com/kicad/code/kicad/doxygen:9.0 /doxygen-python_html /usr/share/nginx/html/doxygen-python-9.0
COPY --from=registry.gitlab.com/kicad/code/kicad-python/kicad-python-docs:main /docs-html /usr/share/nginx/html/kicad-python-main

# change permissions to allow running as arbitrary user
RUN chmod -R 777 /var/log/nginx /var/cache/nginx /var/run \
     && chgrp -R 0 /etc/nginx \
     && chmod -R g+rwX /etc/nginx

# use a different user as open shift wants non-root containers
# do it at the end here as it'll block our "root" commands to set the container up
USER 1000

#expose 8081 as we cant use port 80 on openshift (non-root restriction)
EXPOSE 8081
